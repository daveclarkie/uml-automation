﻿Imports System.Data.SqlClient
Imports cca.common
Imports cca.objects
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Globalization

Imports System.Configuration

Module Helper
    Dim epwd() As Byte = Nothing

    Dim mdt As DataTable
    Dim mdr As DataRow
    Dim dt As DataTable
    Dim dr As DataRow
    Dim cm As SqlClient.SqlCommand
    Dim CDC As DataCommon
    Dim CO As New SqlCommand()


    Dim main_cn As Integer = 1
    Dim hd_cn As Integer = 1

    Sub Main()
        TestLog()
        'Process_NPSSurveys()

        Process_SourcingKPIs("01 June 2018")

    End Sub
    Private Sub TestLog()

        Dim strFile As String = My.Settings.loglocation
        strFile = Replace(strFile, "bin\Debug\", "")
        If Not Directory.Exists(strFile) Then
            Directory.CreateDirectory(strFile)
        End If
        strFile &= "log_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"

        Dim fs As FileStream = Nothing
        If (Not File.Exists(strFile)) Then
            Try
                fs = File.Create(strFile)
                fs.Close()
            Catch ex As Exception

            End Try
        End If

        Dim sw As StreamWriter = File.AppendText(strFile)
        Log("Test Log", sw)

        sw.Close()

    End Sub
    Private Sub Process_NPSSurveys()

        CDC = New DataCommon()

        Dim strFile As String = My.Settings.loglocation
        strFile = Replace(strFile, "bin\Debug\", "")
        If Not Directory.Exists(strFile) Then
            Directory.CreateDirectory(strFile)
        End If
        strFile &= "log_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"

        Dim fs As FileStream = Nothing
        If (Not File.Exists(strFile)) Then
            Try
                fs = File.Create(strFile)
                fs.Close()
            Catch ex As Exception

            End Try
        End If

        Dim sw As StreamWriter = File.AppendText(strFile)
        Log("Started", sw)


        ' **** ICM and Analyst Surveys
        Try

            ' get the list of completed requests from ICM within the last week
            CO = New SqlClient.SqlCommand("ssrs.Survey_ClosedICMRequests")
            CO.CommandType = CommandType.StoredProcedure
            Dim dt As DataTable = CDC.ReadDataTable(CO, hd_cn)

            Log("HD Records: " & dt.Rows.Count, sw)

            ' clear the temp table
            CO = New SqlClient.SqlCommand("TRUNCATE TABLE se_cca.dbo.x_survey_requeststagingareas")
            CO.CommandType = CommandType.Text
            CDC.Execute(CO, main_cn)

            ' import into temp table
            CDC.ExecuteBulkCopy(dt, "se_cca.dbo.x_survey_requeststagingareas", CO, main_cn)

            ' load selected
            CO = New SqlClient.SqlCommand("se_cca.dbo.rsp_helpdeskworkorderdetail_datatable")
            CO.CommandType = CommandType.StoredProcedure
            dt = CDC.ReadDataTable(CO, main_cn)

            ' for each id create icm and analyst survey
            '  get requester and technician id's
            Log("There are " & dt.Rows.Count & " surveys to send today", sw)

            For Each dr As DataRow In dt.Rows
                Log("requester_id: " & dr("requester_id"), sw)
                Dim requester_user As Integer = ReturnInternalUser_fk(dr("requester_id"))
                Log("request_user: " & requester_user, sw)


                Log("technician_id: " & dr("technician_id"), sw)
                Dim technician_user As Integer = ReturnInternalUser_fk(dr("technician_id"))
                Log("technician_user: " & technician_user, sw)

                Dim hd1 As New helpdeskworkorderdetails(35, 35, CDC)
                hd1.helpdesk_id = dr("helpdesk_id")
                hd1.created_time = dr("created_time")
                hd1.requester = dr("requester")
                hd1.requester_id = dr("requester_id")
                hd1.technician = dr("technician")
                hd1.technician_id = dr("technician_id")
                hd1.client = dr("client")
                hd1.tender_type = dr("tender_type")
                hd1.country = dr("country")
                hd1.contract_renewal = dr("contract_renewal")
                hd1.tender_due = dr("tender_due")
                hd1.status = dr("status")
                hd1.commodity = dr("commodity")
                hd1.completed_time = dr("completed_time")
                If hd1.Save Then

                    Dim sd1 As survey1details
                    Dim sd2 As survey2details
                    Dim s As surveys
                    Dim surl As surveyurls
                    Dim url As String = ""
                    Dim body As String = ""
                    Dim subject As String = ""

                    sd1 = New survey1details(35, 35, CDC)
                    sd1.helpdesk_id = hd1.helpdesk_id
                    sd1.selecteduser_fk = requester_user
                    sd1.comments = ""
                    sd1.comments2 = ""

                    If sd1.Save Then
                        s = New surveys(35, 35, CDC)
                        s.surveytype_fk = 1
                        s.surveydetail_fk = sd1.surveydetail_pk
                        s.surveystatus_fk = 1
                        If s.Save Then

                            CO = New SqlCommand
                            CO.CommandType = CommandType.StoredProcedure
                            CO.CommandText = "se_cca.dbo.rsp_surveyurls_generate"
                            CDC.ReadScalarValue(url, CO, main_cn)

                            surl = New surveyurls(35, 35, CDC)
                            surl.survey_fk = s.survey_pk
                            surl.url = url
                            If surl.Save Then
                                Dim requester_email As String = ""
                                CO = New SqlCommand
                                CO.CommandType = CommandType.Text
                                CO.CommandText = "SELECT [se_cca].[dbo].[primaryemailoffice](" & requester_user & ")"
                                CDC.ReadScalarValue(requester_email, CO, main_cn)
                                Log("requester_email: " & requester_email, sw)

                                'requester_email = "dave.clarke@ems.schneider-electric.com"
                                'technician_email = "dave.clarke@ems.schneider-electric.com"

                                subject = "CM Survey for Request# " & hd1.helpdesk_id & " (" & hd1.client & " | " & hd1.commodity & ")"

                                body = "Hello " & hd1.requester & ", <br />"
                                body &= "<br />"
                                body &= "Please could you take a couple of minutes to give some feedback on the Sourcing request that was closed on " & hd1.completed_time.ToLongDateString & "<br /><br />"
                                body &= "<b>Request ID:</b> " & hd1.helpdesk_id & "<br />"
                                body &= "<b>Client:</b> " & hd1.client & "<br />"
                                body &= "<b>URL:</b> <a href='dev.utilitymasters.co.uk/coresurvey/survey.aspx?surveyurl=" & url & "'>LINK</a>"
                                SendSMTP("donotreply@ems.schneider-electric.com", RemoveDiacritics(requester_email), subject, body, "", "HTML")
                                Log("CM Survey (" & url & ") sent to " & requester_email & " | Request ID: " & hd1.helpdesk_id, sw)
                            End If
                        End If
                    End If


                    sd2 = New survey2details(35, 35, CDC)
                    sd2.helpdesk_id = hd1.helpdesk_id
                    sd2.selecteduser_fk = technician_user
                    sd2.comments = ""
                    If sd2.Save Then
                        s = New surveys(35, 35, CDC)
                        s.surveytype_fk = 2
                        s.surveydetail_fk = sd2.surveydetail_pk
                        s.surveystatus_fk = 1
                        If s.Save Then

                            CO = New SqlCommand
                            CO.CommandType = CommandType.StoredProcedure
                            CO.CommandText = "se_cca.dbo.rsp_surveyurls_generate"
                            CDC.ReadScalarValue(url, CO, main_cn)

                            surl = New surveyurls(35, 35, CDC)
                            surl.survey_fk = s.survey_pk
                            surl.url = url
                            If surl.Save Then


                                Dim technician_email As String = ""
                                CO = New SqlCommand
                                CO.CommandType = CommandType.Text
                                CO.CommandText = "SELECT [se_cca].[dbo].[primaryemailoffice](" & technician_user & ")"
                                CDC.ReadScalarValue(technician_email, CO, main_cn)
                                Log("technician_email: " & technician_email, sw)


                                subject = "Analyst Survey for Request# " & hd1.helpdesk_id & " (" & hd1.client & " | " & hd1.commodity & ")"

                                body = "Hello " & hd1.technician & ", <br />"
                                body &= "<br />"
                                body &= "Please could you take a couple of minutes to give some feedback on the Sourcing request that you closed on " & hd1.completed_time.ToLongDateString & "<br /><br />"
                                body &= "<b>Request ID:</b> " & hd1.helpdesk_id & "<br />"
                                body &= "<b>Client:</b> " & hd1.client & "<br />"
                                body &= "<b>URL:</b> <a href='dev.utilitymasters.co.uk/coresurvey/survey.aspx?surveyurl=" & url & "'>LINK</a>"
                                SendSMTP("donotreply@ems.schneider-electric.com", RemoveDiacritics(technician_email), subject, body, "", "HTML")
                                Log("Analyst Survey (" & url & ") sent to " & technician_email & " | Request ID: " & hd1.helpdesk_id, sw)
                            End If
                        End If
                    End If
                End If
                sw.WriteLine("")
            Next

            Log("----------------------------------", sw)

            sw.Close()

        Catch ex As Exception
            Log(ex.Message, sw)
            sw.Close()
        End Try
        CDC = Nothing

    End Sub
    Private Sub Process_SourcingKPIs(ByVal month As String)



        Dim query_value As String = "use servicedesk " & vbCrLf
        query_value &= "declare @StartDate [datetime], @EndDate [datetime]" & vbCrLf
        query_value &= "set @StartDate = '" & month & "'" & vbCrLf
        query_value &= "set @EndDate = '" & month & "'" & vbCrLf
        query_value &= File.ReadAllText("stored_query_sourcingkpi.txt")

        CDC = New DataCommon()

        Dim strFile As String = My.Settings.loglocation & "sourcing_kpi\"
        strFile = Replace(strFile, "bin\Debug\", "")
        If Not Directory.Exists(strFile) Then
            Directory.CreateDirectory(strFile)
        End If
        strFile &= "log_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"

        Dim fs As FileStream = Nothing
        If (Not File.Exists(strFile)) Then
            Try
                fs = File.Create(strFile)
                fs.Close()
            Catch ex As Exception

            End Try
        End If

        Dim sw As StreamWriter = File.AppendText(strFile)
        Log("Started - " & month, sw)


        ' **** ICM and Analyst Surveys
        Try

            Dim startedtime As Date = Now()
            ' get the list of completed requests from ICM within the last week
            CO = New SqlClient.SqlCommand(query_value)
            CO.CommandType = CommandType.Text
            CO.CommandTimeout = 120
            Dim dt As DataTable = CDC.ReadDataTable(CO, hd_cn)
            Dim returnedtime As Date = Now()

            Log("Elapsed: " & DateDiff(DateInterval.Second, startedtime, returnedtime) & " seconds", sw)
            Log("HD Records: " & dt.Rows.Count, sw)

            ' clear the temp table
            CO = New SqlClient.SqlCommand("TRUNCATE TABLE [se_cca].dbo.[sourcing_kpis_temp]")
            CO.CommandType = CommandType.Text
            CDC.Execute(CO, main_cn)

            ' import into temp table
            CDC.ExecuteBulkCopy(dt, "[se_cca].dbo.[sourcing_kpis_temp]", CO, main_cn)

            ' Sort the dates out
            CO = New SqlClient.SqlCommand("EXEC [se_cca].[dbo].[rsp_sourcingkpi_dateupdates]")
            CO.CommandType = CommandType.Text
            CDC.Execute(CO, main_cn)

            ' remove previous export months (overlap)
            CO = New SqlClient.SqlCommand("DELETE FROM [se_cca].[dbo].[sourcing_kpis] WHERE cast(export_month as datetime) IN (SELECT DISTINCT cast(export_month as datetime) FROM [se_cca].dbo.[sourcing_kpis_temp])")
            CO.CommandType = CommandType.Text
            CDC.Execute(CO, main_cn)

            ' load selected
            CO = New SqlClient.SqlCommand("INSERT INTO [se_cca].dbo.[sourcing_kpis] SELECT * FROM [se_cca].dbo.[sourcing_kpis_temp]")
            CO.CommandType = CommandType.Text
            CDC.Execute(CO, main_cn)


            Dim url As String = ""
            Dim body As String = ""
            Dim subject As String = ""

            Dim strURL As String = "http://reports.mceg.local/ReportServer/Pages/ReportViewer.aspx?%2fUML%2fContract+Manager%2fInternal+Reports%2fSourcing+KPI+Report&rs:Command=Render&month=" + month + "&rs:Format=EXCEL"
            Dim strFilePath As String = "\\UK-OL1-FILE-01\Group Shares\Data\Reports\KPI Reporting\Sourcing\" & month & ".xls"


            If Directory.Exists("\\UK-OL1-FILE-01\Group Shares\Data\Reports\KPI Reporting\Sourcing\") Then
                If Not File.Exists(strFilePath) Then
                    MCEGSaveURLtoFile(strURL, strFilePath)
                End If
            Else
                Directory.CreateDirectory("\\UK-OL1-FILE-01\Group Shares\Data\Reports\KPI Reporting\Sourcing\")
                MCEGSaveURLtoFile(strURL, strFilePath)
            End If


            'requester_email = "dave.clarke@ems.schneider-electric.com"
            'technician_email = "dave.clarke@ems.schneider-electric.com"

            subject = "Sourcing Team KPI Report for " & month.Remove(0, 3)

            body = "Hello Matt, <br />"
            body &= "<br />"
            body &= "Sourcing Team KPI report has been run for " & month.Remove(0, 3) & " and there were " & dt.Rows.Count & " new rows.<br /><br />"
            body &= "<b>Please find the report at the following <a href='" & strURL & "' target='_blank'>LINK</a>.</b><br />"
            SendSMTP("donotreply@ems.schneider-electric.com", "matt.sanders@ems.schneider-electric.com", subject, body, "", "HTML")
            Log("Email notification sent to Matt with Report", sw)

            Log("----------------------------------", sw)

            sw.Close()

        Catch ex As Exception
            Log(ex.Message, sw)
            sw.Close()
        End Try
        CDC = Nothing

    End Sub


    Public Sub MCEGSaveURLtoFile(ByVal url As String, ByVal localpath As String)

        Dim loFileStream As New System.IO.FileStream(localpath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim loRequest As System.Net.HttpWebRequest
        Dim loResponse As System.Net.HttpWebResponse
        Dim loResponseStream As System.IO.Stream
        Dim laBytes(256) As Byte
        Dim liCount As Integer = 1
        Try

            loRequest = CType(System.Net.WebRequest.Create(url), System.Net.HttpWebRequest)
            loRequest.Credentials = CredentialCache.DefaultNetworkCredentials 'New System.Net.NetworkCredential(My.Settings.file_username, My.Settings.file_password, My.Settings.file_domain)
            loRequest.Timeout = 1000 * 60 * 15 'timeout 15 minutes
            loRequest.Method = "GET"
            loResponse = CType(loRequest.GetResponse, System.Net.HttpWebResponse)
            loResponseStream = loResponse.GetResponseStream


            Do While liCount > 0
                liCount = loResponseStream.Read(laBytes, 0, 256)
                loFileStream.Write(laBytes, 0, liCount)
            Loop
            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()

        Catch ex As Exception

            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()
        End Try

    End Sub
    Private Function ReturnInternalUser_fk(ByVal helpdeskuserid As Integer) As Integer
        Dim _return As Integer = -1

        Dim hd_loginname As String = ""
        Dim hd_name As String = ""
        Dim se_user_fk As Integer = -1


        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CO.CommandText = "SELECT name FROM [servicedesk].[dbo].[AaaLogin] WHERE USER_ID = " & helpdeskuserid
        CDC.ReadScalarValue(hd_loginname, CO, hd_cn)

        CO = New SqlCommand
        CO.CommandType = CommandType.Text
        CO.CommandText = "SELECT FIRST_NAME FROM [servicedesk].[dbo].[AaaUser] WHERE USER_ID = " & helpdeskuserid
        CDC.ReadScalarValue(hd_name, CO, hd_cn)

        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure
        CO.CommandText = "se_cca.dbo.rsp_helpdesklogindetails_user_fk"
        CO.Parameters.AddWithValue("@username", hd_loginname)
        CDC.ReadScalarValue(se_user_fk, CO, main_cn)

        If se_user_fk = -1 Then
            Dim fullname As String = Replace(hd_name, ".", " ")
            Dim firstName As String = StrConv(fullname.Substring(0, fullname.IndexOf(" ")), vbProperCase)
            Dim lastName As String = StrConv(fullname.Substring(fullname.IndexOf(" ") + 1), vbProperCase)

            se_user_fk = CreateNewPerson(firstName, lastName, hd_loginname, helpdeskuserid, "None", 0, 0)
        End If

        _return = se_user_fk
        Return _return
    End Function
    Private Function CreateNewPerson(ByVal forename As String, ByVal surname As String, ByVal hdusrname As String, ByVal hdusrid As Integer, ByVal office As String, Optional ByVal readlevel As Integer = 1, Optional ByVal writelevel As Integer = 1, Optional ByVal password As String = "") As Integer
        Dim username As String = Nothing
        Dim usrname As String = ""
        Dim user_fk As Integer = -1

        Dim CO As New SqlCommand
        CO = New SqlCommand
        CO.CommandType = CommandType.StoredProcedure

        CO.CommandText = "se_cca.dbo.rsp_CreateUserName"
        CO.Parameters.AddWithValue("@fn", forename.ToLower)
        CO.Parameters.AddWithValue("@sn", surname.ToLower)
        CDC.ReadScalarValue(username, CO, main_cn)
        CO.Parameters.Clear()

        If Not username Is Nothing Then

            If usrname.Length > 0 Then : username = usrname : End If

            If password.Length < 5 Then
                CO.CommandText = "se_cca.dbo.rsp_CreatePasswd"
                CDC.ReadScalarValue(password, CO, main_cn)
                CO.Parameters.Clear()
            End If

            CO.CommandText = "se_cca.dbo.rsp_CreatePasswdEnc"
            CO.Parameters.AddWithValue("@pwd", password)
            CDC.ReadScalarValue(epwd, CO, main_cn)
            CO.Parameters.Clear()

            Dim u As New users(-1, -1, CDC)
            u.username = username
            u.encpassword = epwd
            u.readlevel = readlevel
            u.writelevel = writelevel
            If u.Save() Then

                user_fk = u.user_pk

                Dim ug As New usergroups(u.user_pk, u.user_pk, CDC)
                ug.user_fk = u.user_pk
                ug.group_fk = 4
                ug.Save()

                Dim p As New persons(u.user_pk, u.user_pk, CDC)
                p.forename = forename
                p.surname = surname
                p.user_fk = u.user_pk
                p.middlename = ""
                p.jobtitle = ""
                If p.Save() Then
                    Dim e As New emails(u.user_pk, u.user_pk, CDC)
                    e.email = Replace(forename.ToLower & "." & surname.ToLower, " ", ".") & "@ems.schneider-electric.com"
                    If e.Save() Then
                        Dim ue As New useremails(u.user_pk, u.user_pk, CDC)
                        ue.user_fk = u.user_pk
                        ue.email_fk = e.email_pk
                        ue.contactlocation_fk = 2
                        If ue.Save() Then
                            If Not office = "None" Then
                                Dim l As New locations(u.user_pk, u.user_pk, CDC)
                                Select Case office
                                    Case "Oldham" : l.Read(1)
                                    Case "Dunfermline" : l.Read(2)
                                    Case "Budapest" : l.Read(3)
                                End Select
                                Dim ul As New userlocations(u.user_pk, u.user_pk, CDC)
                                ul.user_fk = u.user_pk
                                ul.location_fk = l.location_pk
                                ul.locationuse_fk = 1
                                If ul.Save() Then
                                    Dim ph As New phones(u.user_pk, u.user_pk, CDC)

                                    Select Case office
                                        Case "Oldham" : ph.Read(2)
                                        Case "Dunfermline" : ph.Read(3)
                                        Case "Budapest" : ph.Read(4)
                                    End Select
                                    Dim uph As New userphones(u.user_pk, u.user_pk, CDC)
                                    uph.user_fk = u.user_pk
                                    uph.phone_fk = ph.phone_pk
                                    uph.contactlocation_fk = 2
                                    uph.Save()
                                End If
                            End If
                        End If
                    End If
                End If

                Dim hdu As New helpdesklogindetails(u.user_pk, u.user_pk, CDC)
                hdu.user_fk = u.user_pk
                hdu.username = hdusrname
                hdu.password = "* missing *"
                hdu.helpdeskdomain_fk = 1
                hdu.helpdesklogondomain_fk = 1
                hdu.helpdeskuserid = hdusrid
                hdu.Save()
            End If

            Dim strBody As String = ""

            strBody &= "UserID: " & u.user_pk & "<br />"
            strBody &= "Username: " & u.username & "<br />"
            strBody &= "Password: " & password & "<br />"
            strBody &= "URL: <a href='dev.utilitymasters.co.uk'>dev.utilitymasters.co.uk</a>"


            SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "New User Created", strBody, "", "HTML")
        Else
            CO = New SqlCommand
            CO.CommandType = CommandType.StoredProcedure
            CO.CommandText = "se_cca.dbo.rsp_helpdesklogindetails_user_fk"
            CO.Parameters.AddWithValue("@username", hdusrname)
            CDC.ReadScalarValue(user_fk, CO, main_cn)

        End If



        Return user_fk
    End Function
    Private Sub SendSMTP(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String, Optional ByVal format As String = "Text", Optional ByVal BCC As String = "", Optional ByVal User As String = "", Optional ByVal Pass As String = "")
        Dim htmlMessageBody As String
        htmlMessageBody = " "
        htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
        htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
        htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
        htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
        htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
        htmlMessageBody = htmlMessageBody + " </STYLE>"
        htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
        htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
        htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
        htmlMessageBody = htmlMessageBody + " <P>"
        htmlMessageBody = htmlMessageBody + strBody
        htmlMessageBody = htmlMessageBody + "</P>"
        htmlMessageBody = htmlMessageBody + " </TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
        htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD></TD>"
        htmlMessageBody = htmlMessageBody + " <TD>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
        htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
        htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
        htmlMessageBody = htmlMessageBody + " <TBODY>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
        htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
        htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
        htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
        htmlMessageBody = htmlMessageBody + " <TR>"
        htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"



        Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
        With insMail
            .Subject = strSubject
            If format = "HTML" Then : .IsBodyHtml = True : Else : .IsBodyHtml = True : End If
            .Body = htmlMessageBody
            If BCC.Length > 0 Then
                .Bcc.Add(New MailAddress(BCC))
            End If
            .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
            If Not strAttachments.Equals(String.Empty) Then
                Dim strFile As String
                Dim strAttach() As String = strAttachments.Split(";"c)
                For Each strFile In strAttach
                    .Attachments.Add(New System.Net.Mail.Attachment(strFile.Trim()))
                Next
            End If
        End With
        Dim smtp As New System.Net.Mail.SmtpClient

        If User.Length > 0 Then
            smtp.Credentials = New NetworkCredential(User, Pass)
        Else
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials
        End If

        CO = New SqlCommand("se_cca.dbo.rsp_systemparameters_latest")
        CO.CommandType = CommandType.StoredProcedure
        Dim systemparameter_pk As Integer = -1
        CDC.ReadScalarValue(systemparameter_pk, CO, main_cn)
        Dim sysParam As New systemparameters(-1, -1, systemparameter_pk, CDC)

        smtp.Host = sysParam.smtpserver
        smtp.Port = 25
        smtp.Send(insMail)
    End Sub
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then cnt += 1
        Next
        Return cnt
    End Function
    Public Sub Log(logMessage As String, w As TextWriter)
        Console.WriteLine(logMessage)
        w.Write(vbCrLf)
        w.Write("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString())
        w.Write(" : {0}", logMessage)
    End Sub
    Private Function RemoveDiacritics(text As String) As String
        Dim normalizedString = text.Normalize(NormalizationForm.FormD)
        Dim stringBuilder = New StringBuilder()

        For Each c As Char In normalizedString
            Dim unicodeCategory__1 = CharUnicodeInfo.GetUnicodeCategory(c)
            If unicodeCategory__1 <> UnicodeCategory.NonSpacingMark Then
                stringBuilder.Append(c)
            End If
        Next

        Return stringBuilder.ToString().Normalize(NormalizationForm.FormC)
    End Function

    Public Function FirstDayOfWeek(year As Integer, month As Integer, Day As DayOfWeek) As DateTime
        Dim firstDay As New DateTime(year, month, 1)
        If firstDay.DayOfWeek = Day Then
            Return firstDay
        Else
            Dim days = Day - firstDay.DayOfWeek ' in case the first day was less than DayOfWeek
            If firstDay.DayOfWeek > Day Then days = 7 - firstDay.DayOfWeek + Day ' in case if the first day was > DayOfWeek             
            Return firstDay.AddDays(days)
        End If
    End Function

End Module